Copyright (c) 2018 Teragence <info@teragence.com>

		Teragence END USER LICENCE AGREEMENT
Definitions
"Content" means the audio, text, images, video, other multimedia content, other information or material You provide or transmit when using the Software;
“Licence Agreement or EULA” means these licence terms governing use of the Software;
“Permitted Purpose” means using the Software for purposes as indicated on the Licensor’s website or the description accompanying the Software. 
“Software” means the Teragence SDK or mobile application; 
"Privacy Policy" means the privacy policy available at the Public App Store or the Licensor’s website which governs how Licensor processes any personal data collected from You;
“Public App Store” means the public Google Play Store, Apple iTunes Store, Windows Phone Store or the Amazon Appstore.
"Licensor " means Teragence Ltd and its licensors. A reference to us in these terms also includes our group companies from time to time. 
"You or your" means the person accessing or using the Software or Content. 

A.	Property of Licensor
You may obtain a copy of this software product either by downloading it remotely from the app store, our servers or other media (“Downloaded File”). The copyright, database rights and any other intellectual property rights in the programs and data which constitute this software product (the “Software”), together with the Downloaded File on which they were supplied to You, are and remain the property of Licensor. You are licensed to use them only if You accept all the terms and conditions set out below.
B.	Licence Acceptance Procedure
By clicking on the acceptance button which follows this Licence Agreement, You indicate acceptance of this Licence Agreement and limitation of liability set out in this Licence Agreement. Such acceptance is either on Your own behalf of on behalf of any corporate entity which employs You or which You represent (“Corporate Licensee”). In this Licence Agreement, 'You' and 'Your' include both the reader and any Corporate Licensee.
C.	Licence Rejection Procedure
You should therefore read this Licence Agreement carefully before clicking on the acceptance button. If You do not accept these terms and conditions, You should not install the Software on Your mobile phone or computer and promptly (and in any event, within 14 days of receipt) (a) delete any Downloaded File (b) return any other items provided that are part of this product; and (c) provide Your dated proof of purchase. Any money You paid to the Licensor or a licensed reseller for the Software will be refunded (if applicable). 
D.	Other Agreements
If Your use of these programs and data is pursuant to an executed Licence Agreement, such agreement shall apply instead of the following terms and conditions. You also agree to that Your use of the Software is subject to Teragence Ltd’s Privacy Policy. 
1.	Intellectual Property / Ownership
The Software and all intellectual property rights in it including the Software are owned by Licensor and its licensors. Intellectual property rights means rights such as: copyright, trade marks, domain names, design rights, database rights, patents and all other intellectual property rights of any kind whether or not they are registered or unregistered (anywhere in the world). You agree not to adjust, to try to circumvent, or delete any notices contained on the Software. You hereby grant Licensor a royalty free, non-exclusive, irrevocable, worldwide licence to use any Content for the Permitted Purpose. The Software and related documentation are copyrighted works of authorship, and are also protected under applicable database laws. The Licensor retains ownership of the Software and all subsequent copies of the Software, regardless of the form in which the copies may exist. This licence is not a sale of the original software or any copies. 
2.	Fees and Payments
You are entitled to use the Software subject to payment of fees and commissions in accordance with the pricing schedule shown on the Software or the Licensor’s website, as amended from time-to-time (if applicable). Any password, licence key or similar installation or usage control codes provided by the Licensor to You is considered the confidential information of Licensor, and You must hold such information in strict confidence. Licensor reserves the right to charge for updates, and ongoing support & maintenance. Licensor shall be permitted to recover any legal and enforcement costs incurred in collecting fees owed. 
3.	Licence
Provided that the applicable fees have been paid (if applicable), the Licensor grants to You a limited, non-exclusive licence to use the Software for the Permitted Purpose accordance with the applicable pricing schedule provided by the Licensor from time-to-time. If you do not pay fees in accordance with the pricing schedule (if applicable), You cannot use the Software. 
4.	Licence Restrictions 
When using the Software you agree not to:  
a.	Assist in disruption of the Software or the servers and networks which are connected to the Software, or breach and rules or regulations of the networks connected to the Software;
b.	Use the Software for a fraudulent or illegal purpose, or to use the Software to harvest personal data without prior consent;
c.	Use the Software to upload, download, send, transmit or publish any Content that infringes on the intellectual property rights of any owner;
d.	Use the Software to upload, download, send, transmit or publish any software viruses or malicious code;
e.	Use the Software in a manner that is unlawful, threatening, obscene, defamatory, libelous, harassing, hateful, racially offensive, or encourages conduct that would be considered a criminal offense, gives rise to civil liability, violate any law, or is otherwise inappropriate; or
f.	Provide false, inaccurate or misleading details or misrepresent your affiliation with any party. 
g.	attempt to gain unauthorised access to websites of Teragence or their users, customers or partners, or any related networks or systems.
h.	violate (or assist any other party in violating) any applicable laws or regulations. 
i.	conduct any transaction dealing with the proceeds of illegal activity. 
j.	try to defraud Teragence Limited or any users of the Software. 
5.	Acknowledgements 
You hereby acknowledge and agree to the following:
a.	You may not use, copy, modify or transfer the Software (including any related documentation) or any copy, in whole or in part, including any print-out of all or part of any database, except as expressly provided for in this licence. If You transfer possession of any copy of the Software to another party except as provided above, Your licence is automatically terminated. You may not translate, reverse engineer, decompile, disassemble, modify or create derivative works based on the Software, except as expressly permitted by law. You may not vary, delete or obscure any notices of proprietary rights or any product identification or restrictions on or in the Software.
b.	You agree not to not to, translate, merge, adapt, vary, alter or modify, the whole or any part of the Software nor permit the Software or any part of it to be combined with, or become incorporated in, any other programs, except as expressly permitted in this Licence Agreement. 
c.	You agree not to use the Software via any communications network or by means of remote access. Third parties (outside the Corporate Licensee) shall not be permitted to access or use the Software, and including any use in any application service provider environment, service bureau, software-as-as-service or time-sharing arrangements.
e.	You acknowledge it is your responsibility to determine what, if any, taxes apply to the payments you make or receive, and to collect, report, and provide the correct tax to the appropriate tax authority. Licensor will make any tax withholdings or filings that we are required by law to make, but Licensor is not responsible for determining whether taxes apply to your transaction, or for collecting, reporting, or remitting any taxes arising from any transaction.
f.	You acknowledge that Licensor has the right to change, suspend, or discontinue any aspect of the Software at any time, including hours of operation or availability of any feature, without notice and without liability.
g.	You acknowledge that Licensor may apply limits to the value, speed and frequency of transactions You can make using the Software. Licensor reserves the right to change the amounts of such limits if it deems it reasonably necessary. 
i.	You acknowledge that Licensor may suspend or close your access to the Software at any time.
j.	You may terminate this Licence Agreement at any time by discontinuing use of the Software. 
5.	No Transfer 
The Software is licensed only to You. You may not rent, lease, loan, sub-license, sell, assign, pledge, transfer or otherwise dispose of the Software, on a temporary or permanent basis, without the prior written consent of the Licensor.
6.	Undertakings
You undertake to:
a.	ensure that, prior to use of the Software by Your employees or agents, all such parties are notified of this licence and the terms of this Agreement;
b.	reproduce and include our copyright notice (or such other party's copyright notice as specified on the Software) on all and any copies of the Software, including any partial copies of the Software;
c.	hold all drawings, specifications, data (including object and source codes), software listings and all other information relating to the Software confidential and not at any time, during this licence or after its expiry, disclose the same, whether directly or indirectly, to any third party without the Licensor's consent.
7.	Limitation of Liability
To the fullest extent permitted by law our liability is excluded for loss, injury or damage (whether direct or indirect or consequential or incidental or special) arising out of or in connection with your use of the Software or Content including without limitation any and all losses relating to or resulting from:
•	their accuracy, reliability, completeness, suitability, merchantability or fitness for purpose;
•	any reliance upon or use of or actions taken or not taken or decisions made on the basis of anything contained therein;
•	inability at any time to obtain access to any part of the Software or the Licensor’s website; or
•	any computer viruses or spyware or malware of any description or any material which might adversely affect the operation of any computer hardware or software or any communications network which affects you as a result of you accessing the Software,
The above provisions do not affect our liability for death or personal injury arising from our negligence, fraud or fraudulent misrepresentation or for any other liability which is not permitted to be excluded or limited by applicable laws.
8.	Indemnity
You agree to hold harmless, indemnify and defend the Licensor, its officers, directors, employees and third party suppliers against any loss, damage, fine, or expense, including legal fees arising out of or related to any claim that you have used this Software in violation of this Licence Agreement, the applicable laws in your jurisdiction or third party intellectual property rights. 
9.	Your Statutory Rights
This licence gives You specific legal rights and You may also have other rights that vary from country to country. Some jurisdictions do not allow the exclusion of implied warranties, or certain kinds of limitations or exclusions of liability, so the above limitations and exclusions may not apply to You. Other jurisdictions allow limitations and exclusions subject to certain conditions. In such a case the above limitations and exclusions shall apply to the fullest extent permitted by the laws of such applicable jurisdictions. If any part of the above limitations or exclusions is held to be void or unenforceable, such part shall be deemed to be deleted from this Agreement and the remainder of the limitation or exclusion shall continue in full force and effect. Any rights that You may have as a consumer (i.e. a purchaser for private as opposed to business, academic or government use) are not affected. 
10.	Term
The licence is effective until terminated by either party or at expiry of the applicable licence term. If Licensor wishes to terminate the licence prior to the applicable licence term, it may do so by providing notice in writing, and any money You paid to the Licensor or a licensed reseller for the Software will be refunded. You may terminate the licence at any time by destroying the Software together with all copies in any form. The licence will also terminate upon conditions set out elsewhere in this Agreement or if You fail to comply with any term or condition of this Agreement or if You fail to pay any charges owing in respect of the Software (including support and maintenance, if applicable) or if You voluntarily return the Software to us. You agree upon such termination to destroy the Software together with all copies in any form.
So that there is no uncertainty, termination of the licence does not remove, limit or otherwise affect any rights, obligations, demands, claims or remedies that have arisen under this Agreement prior to termination. Those rights, obligations, demands, claims and remedies will remain in effect despite termination of the licence.
Licensor shall have the right to terminate the licence without a refund obligation, if Licensor becomes aware of evidence satisfactory, in Licensor's sole discretion, to establish that the Software is being used without authorisation by a party other than the Licensee.  
The provisions regarding Intellectual Property, Licence Restrictions and Acknowledgements, Limitation of Liability, Indemnity and Governing Law shall survive termination of the Licence Agreement. 
11.	General 
You agree that the Licensor shall have the right, after supplying undertakings as to confidentiality, to audit any computer system on which the Software are installed in order to verify compliance with this Licence Agreement. 
Each party irrevocably agrees that the Courts of England shall have non-exclusive jurisdiction to resolve any controversy or claim of whatever nature arising out of or in relation to this Licence Agreement and the laws of England and Wales shall govern such controversy or claim.
This Licence Agreement constitutes the complete and exclusive statement of the Licence Agreement between the Licensor and You with respect to the subject matter of this Licence Agreement and supersedes all proposals, representations, understandings and prior agreements, whether oral or written, and all other communications between us relating to that subject matter.
Any clause in this Licence Agreement that is found to be invalid or unenforceable shall be deemed deleted and the remainder of this Agreement shall not be affected by that deletion.
Failure or neglect by either party to exercise any of its rights or remedies under this Agreement will not be construed as a waiver of that party's rights nor in any way affect the validity of the whole or part of this Licence Agreement nor prejudice that party's right to take subsequent action.
This Licence Agreement is personal to You and You may not assign, transfer, sub-contract or otherwise part with this Licence Agreement or any right or obligation under it without the Licensor's prior written consent. The Licensor shall be permitted to assign this Licence Agreement without your consent upon notice. 
Should You have any questions concerning this Agreement, please contact the Licensor.
This Licence Agreement is dated 19 April 2018.
