//
//  TRGController.h
//  SDK_Teragence
//
//  Created by Teragence on 9/11/17.
//  Copyright © 2017 NLT. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSUInteger, TestOption) {
    TestOptionDownload = 1 << 0,
    TestOptionBurst = 1 << 1,
    TestOptionPing = 1 << 2,
    TestOptionRegister = 1 << 3
};

@interface TRGController : NSObject

@property (nonatomic) BOOL isActive;
@property (nonatomic) BOOL enableBackgroundMode;

- (instancetype)initWithPartnerID:(NSString *)partnerID;

- (instancetype)initWithPartnerID:(NSString *)partnerID error:(NSError**)error;

- (void)makeManualMesurements __deprecated_msg("This method has been renamed to makeManualMeasurements.");

- (void)makeManualMeasurements;
@end

