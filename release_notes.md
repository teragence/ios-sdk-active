 Version	   |     Description
 ------------- | --------------
 2.0.9      |   Fixed a bitcode issue in the previous release.
 2.0.8      |   Bug fixes and stability improvements.
 2.0.7      |   Bug fixes and stability improvements.
 2.0.6      |   Bug fixes and stability improvements.
 2.0.5      |   Bug fixes and stability improvements.
 2.0.4      |   Bug fixes and stability improvements.
 2.0.3      |   Bug fixes and stability improvements.
 2.0.2      |   Bug fixes and stability improvements.
 2.0.1      |   Bug fixes and stability improvements.
